# SML/NJ

Standard ML compiler from New Jersey. https://smlnj.org

## Unofficial documentation
* [repology](https://repology.org/project/smlnj/versions)
* WikipediA
  * [*Standard ML of New Jersey*](https://en.wikipedia.org/wiki/Standard_ML_of_New_Jersey)
  * [*Standard ML*](https://en.wikipedia.org/wiki/Standard_ML)
* [*Standard ML and how I’m compiling it*
  ](https://thebreakfastpost.com/2015/06/10/standard-ml-and-how-im-compiling-it/)
  2015 Chris Cannam

## Projects using neighbour languages
* ML
  * Standard ML
    * MLton
      * [pacman-packages-demo/mlton](https://gitlab.com/pacman-packages-demo/mlton)
    * Poly/ML
      * [pacman-packages-demo/polyml](https://gitlab.com/pacman-packages-demo/polyml)
  * OCaml
    * [pacman-packages-demo/ocaml](https://gitlab.com/pacman-packages-demo/ocaml)
    * [ocaml-packages-demo](https://gitlab.com/ocaml-packages-demo)
* Mercury
    * [apt-packages-demo/mercury](https://gitlab.com/apt-packages-demo/mercury)